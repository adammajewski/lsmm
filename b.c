/*


gcc -std=c99 -Wall -Wextra -pedantic -O3 b.c
./a.out > a.pgm

program from :
http://mathr.co.uk/blog/2014-03-06_mandelbrot_notebook.html
How to write a book about the Mandelbrot set by 
Claude Heiland-Allen
October 18, 2014

with small changes 


LSM/M

Level Set Method of Escape Time 
for drawing Mandelbrot set
using gray gradient 

*/


#include <complex.h>
#include <stdio.h>

// speed up computations 
float cabs2(complex float z) {
return creal(z) * creal(z) + cimag(z) * cimag(z);
}
 
complex float coordinate(int i, int j, int width, int height, complex float center, float radius) {
  float x = (i - width /2.0) / (height/2.0);
  float y = (j - height/2.0) / (height/2.0);
  complex float c = center + radius * (x - I * y);
  return c;
}

int calculate(complex float c, int maximum_iterations, float escape_radius2) {
  complex float z = 0;
  int final_n = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    z = z * z + c;
    if (cabs2(z) > escape_radius2) {
      final_n = n;
      break;
    }
  }
  return final_n;
}


void render(int width, int height, complex float center, float radius, int maximum_iterations, float escape_radius2) {
  printf("P5\n%d %d\n255\n", width, height);
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      complex float c = coordinate(i, j, width, height, center, radius);
      int final_n = calculate(c, maximum_iterations, escape_radius2);
      if ( final_n>0) final_n = 255-final_n; // inverse gradient when exterior 
      // mandelbrot set is black : final_n = 0  
      putchar(final_n);          
    }
  }
}



int main() {

//
int iWidth =1000;
int iHeight = 1000;
// 
float complex center= -0.75; // 
float radius = 1.5;

//
int maximum_iterations=1000;

float escape_radius = 2.0;
float escape_radius2;





  escape_radius2= escape_radius*escape_radius;

  render(iWidth, iHeight, center, radius, maximum_iterations, escape_radius2); 

   return 0;

}
